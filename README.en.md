OpenLooKeng on YARN 
===================
The OpenLooKeng-on-YARN deployment package provides automation scripts and configuration templates
to deploy openLooKeng clusters on YARN.

### Features

1. Launch multiple OpenLooKeng nodes on a single machine
1. Deploy multiple clusters
1. Scale up/down OLK workers in a cluster
1. Query OpenLooKeng cluster status
1. Tear-down OpenLooKeng cluster
1. High availability deployment with multiple coordinators, using Nginx as a load balancer

### Pre-requisites

1. A Hadoop environment with YARN service framework.
    1. Apache Hadoop 3.1.0 and later
    1. User access right to HDFS
1. Environment Requirements
    1. Python3, version 3.6 or higher
        1. hdfs
        2. requests
        3. hdfs[kerberos]
        4. requests_kerberos
    
### OLK cluster on YARN
The ./bin/olk_on_yarn.py script provides one-stop deployment management for OLK cluster on YARN

### Administrator Configuration

The following configuration files would be edited by the administrator to set the correct environment files and also the
cluster configurations.

#### HDFS Filesystem - Mandatory

Correct configurations will be required for OLK instances to access the HDFS data. Administrator will be required to provide the
correct configurations for the following files. Under the **conf/etc_template/filesystem/** directory

1. core-site.xml
2. hdfs-site.xml

#### Deployment

Automation scripts can be configured to provide parameters such as
1. OLK Cluster name and service port
1. Number of workers and/ or coordinators
1. High Availability enable/ disable
1. YARN Resource Manager socket address

Example configuration for automation scripts
- olk_on_yarn_default.json (default)

Provide configuration files for the OLK coordinator under the following directory:

- conf/etc_template/coordinator

Provide configuration files for the OLK worker under the directory

- conf/etc_template/worker

#### Plugins & Catalogs

Additional configuration files for OpenLooKeng should be provided in the 
coordinator and worker directories.

#### Derivative work

The original copy of bin/launcher.py can be obtained from the airlift/airlift project
https://github.com/airlift/airlift/blob/master/launcher/src/main/scripts/bin/launcher.py

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
